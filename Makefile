.PHONY: all
fjh:book/de_inputs.tex
fjh:book/en_inputs.tex
fjh:book/save_or_die_inputs.tex
fjh:book/unauthorized_ending_inputs.tex

book/de_inputs.tex: $(wildcard book/de/*.tex)
	ls -v book/de/*.tex | awk '{printf "\\input{%s}\n", $$1}' > book/de_inputs.tex
book/en_inputs.tex: $(wildcard book/en/*.tex)
	ls -v book/en/*.tex | awk '{printf "\\input{%s}\n", $$1}' > book/en_inputs.tex
